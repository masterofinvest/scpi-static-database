# LouveInvest OpenDatabase by MasterOfInvest

This project is updated every day. And based on LouveInvest's Google Sheet OpenData (thanks to them) we encourage you to contribute to them.

However, you can fork this project at any time if needed, but you will then be out of sync : https://docs.google.com/spreadsheets/d/1McJtTdsId_xjdpAV7WQbj6COlz5yCOOIiJsIj45tpcU/edit#gid=1312623271

## How to use

### Hosted

You can run GET request at http://masterofinvest.evkoh.org:8081/?path=...

```javascript
async getScpis() {
    return await (await fetch('http://masterofinvest.evkoh.org:8081/?path=built/scpis/full-override.json')).json();
}

that.getScpis().then((scpis) => {
    console.log(scpis);
});
```

For the inflation http://masterofinvest.evkoh.org:8081/?path=built/inflation/france.json

Please save this data in your localstorage or cookies for 24 hours

You can display the json in a formatted way (.pretty.json) : http://masterofinvest.evkoh.org:8081/?path=built/scpis/full-override.pretty.json
